const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const postRoutes = require('./routes/posts.js');

// import postRoutes from './routes/posts.js';

//initialize app
const app = express();

app.use('/posts', postRoutes);

//limit file image up to 30mb
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());


//mongodb
//https://www.mongodb.com/cloud/atlas
const username = 'mernmongodb';
const password = 'v9MkKeo7bDAHyl7H'
const CONNECTION_URL = `mongodb+srv://${username}:${password}@cluster0.0yl2e.mongodb.net/<dbname>?retryWrites=true&w=majority`;

const PORT = process.env.PORT || 5000;

mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message));

mongoose.set('useFindAndModify', false);