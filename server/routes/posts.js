const express = require('express');
const router = express.Router();

const controller = require('../controllers/posts.js');

// router.get('/', (req, res) => {
//     res.send('this works');
// });
router.get('/', controller.getPosts);
router.post('/', controller.createPosts);

// export default router;
module.exports = router;